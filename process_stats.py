import aug_util as aug
import wv_util as wv
import matplotlib.pyplot as plt
import numpy as np
import csv
import scipy.misc
import operator
import glob
import os
import json

print('Loading GeoJSON...')
coords, chips, classes = wv.get_labels('xView_train.geojson')
print('Loaded GeoJSON!')

# This is where xView splits its data. Taken from:
# https://github.com/DIUx-xView/baseline/blob/master/scoring/score.py#L271

splits = {
  'map/small': [17, 18, 19, 20, 21, 23, 24, 26, 27, 28, 32, 41, 60,
                   62, 63, 64, 65, 66, 91],
  'map/medium': [11, 12, 15, 25, 29, 33, 34, 35, 36, 37, 38, 42, 44,
                  47, 50, 53, 56, 59, 61, 71, 72, 73, 76, 84, 86, 93, 94],
  'map/large': [13, 40, 45, 49, 51, 52, 54, 55, 57, 74, 75, 77, 79, 82, 83, 89],

  'map/common': [13,17,18,19,20,21,23,24,25,26,27,28,34,35,41,
                  47,60,63,64,71,72,73,76,77,79,83,86,89,91],
  'map/rare': [11,12,15,29,32,33,36,37,38,40,42,44,45,49,50,
                  51,52,53,54,55,56,57,59,61,62,65,66,74,75,82,84,93,94]
}

splits['map/all'] = splits['map/small'] + splits['map/medium'] + splits['map/large']


# Load the class number -> class string label map
labels = {}
with open('xview_class_labels.txt') as f:
    for row in csv.reader(f):
        labels[int(row[0].split(":")[0])] = row[0].split(":")[1]
        
with open('class-splits.json', 'w') as fp:
    json.dump(splits, fp, indent=4)

# Data is in xmin,ymin,xmax,ymax format. Returns (width, height) tuple.
def get_dims_from_coord(coord):
    return (coord[2] - coord[0], coord[3] - coord[1])

def get_area_from_coords(coord):
    dims = get_dims_from_coord(coord)
    return dims[0] * dims[1]

def process_stats_on_array(arr, weights=None):
    return {
        'mean': np.average(arr, weights=weights),
        'median': np.median(arr),
        'std': np.std(arr),
        'var': np.var(arr),
        'range': np.ptp(arr),
        'min': np.amin(arr),
        'max': np.amax(arr),
        'length': len(arr),
        'min_likely_area': max(0, np.average(arr, weights=weights) - 2 * np.std(arr)),
        'max_likely_area': np.average(arr, weights=weights) + 2 * np.std(arr)
    }

# Computes statistics for areas

area_averages = {}

for idx, coord in enumerate(coords):
    area = get_area_from_coords(coord)
    
    # Add new class, if it doesn't already exist
    #
    if int(classes[idx]) not in area_averages:
        area_averages[int(classes[idx])] = {
            'class': classes[idx],
            'values': []
        }
        
    area_averages[int(classes[idx])]['values'].append(area)
    if classes[idx] in labels:
        area_averages[int(classes[idx])]['name'] = labels[classes[idx]]
    else:
        area_averages[int(classes[idx])]['name'] = str(classes[idx])
        
# Compute average areas
for class_id in area_averages.keys():
    area_averages[int(class_id)]['stats'] = process_stats_on_array(area_averages[class_id]['values'])
    
print('Done processing.')

# Calculate statistics for the computed array of statistics (mean-of-means, etc.)

lengths = list(map(lambda key: area_averages[key]['stats']['length'], area_averages.keys()))
meta_stats_of_interest = ['mean', 'median']
stats = {}

for stat in meta_stats_of_interest:
    
    stat_list = list(map(lambda key: area_averages[key]['stats'][stat], area_averages.keys()))
    
    # Weight the averages by number of occurences of objects. This should give us a better idea of the
    # actual data spread.
    stats[stat] = process_stats_on_array(stat_list, weights=lengths)
    
with open('class-statistics.json', 'w') as fp:
    truncated_stats = dict(area_averages)
    for stat in truncated_stats.keys():
        truncated_stats[stat].pop('values', None)
    json.dump(truncated_stats, fp, indent=4)

# Plot averages (mean and median)
def plot_means(avg_dict, title='Class Object Area Average', stat_type='mean'):
    plot_classes = []
    plot_means = []

    # Gotta do some cool workarounds because we want a nice, sorted plot.
    reduced_for_plot = {}

    for class_id in avg_dict.keys():
        reduced_for_plot[area_averages[class_id]['name'] + '(%s)' % avg_dict[class_id]['stats']['length']] = avg_dict[class_id]['stats'][stat_type]
    sorted_x = sorted(reduced_for_plot.items(), key=operator.itemgetter(1))

    # Example data
    x,y = zip(*sorted_x)
    x_pos = np.arange(len(x))

    plt.figure(figsize=(15,10))
    plt.bar(x_pos, y, align='center', alpha=0.4)
    plt.xticks(x_pos, x, rotation='vertical')
    plt.ylabel('Mean')
    plt.title(title)

    plt.show()
